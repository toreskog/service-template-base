
module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
  },
  extends: 'airbnb-base',
  env: {
    node: true
  }
}
