
import { UserService } from '../user';

class AuthMiddleware {
  static async deserializeUser(req, res, next) {
    const { user } = req.session;
    if (user) {
      req.session.user = await UserService.findById(user.id);
    }
    next();
  }
}

export default AuthMiddleware;
