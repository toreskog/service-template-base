
import express from 'express';
import session from 'express-session';
import RedisStore from 'connect-redis';
import bodyParser from 'body-parser';

import Auth, { AuthMiddleware } from './auth';
import Database from './database';
import logger from './logger';

/**
 * Handle exit
 */
process.on('exit', message => logger.info(`About to exit with message: ${message}`));

/**
 * Setup server
 */
const app = express();
const host = process.env.HOST || '0.0.0.0';
const port = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(session({
  store: new (RedisStore(session))({
    host: 'redis',
  }),
  secret: process.env.SESSION_KEY,
  resave: false,
  saveUninitialized: false,
}));
app.use(AuthMiddleware.deserializeUser);
app.set('port', port);

/**
 * Init database
 */
Database.init()
  .then(async () => {
    /**
     * Init modules
     */
    await Auth.init(app);

    /**
     * Handle serverErrors
     */
    app.use((err, req, res, next) => {
      if (err) {
        return res.status(err.status).json({
          message: err.message,
          code: err.code,
          errors: err.errors,
        });
      }

      return next();
    });

    /**
     * Start server
     */
    app.listen(port, host);
    logger.info(`Server listening on ${host}:${port}`);
  });
