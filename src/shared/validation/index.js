import spected from 'spected';

const isRequired = value => value.length > 0;
const isRequiredMsg = field => `${field} is required`;

// eslint-disable-next-line no-useless-escape
const isEmail = email => /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);

const signupConstraints = {
  email: [
    [isEmail, 'Email is not valid'],
  ],
  password: [
    [isRequired, isRequiredMsg('password')],
  ],
};

const validate = (obj, constraints) => {
  const errors = spected(constraints, obj);
  Object.entries(errors).forEach(([key, value]) => {
    if (value === true) delete errors[key];
  });

  if (Object.keys(errors).length) {
    return errors;
  }

  return null;
};

export { signupConstraints };
export default validate;
